from django.urls import path
from . import views

urlpatterns = [
    path('', views.index), #cuando se pida el recurso / de la app, se va al
    #fichero views y al método index. Las peticiones primero la maneja el urls.py
    #del proyeto, y luego se las pasará según corresponda o no, al urls.py de calc
    path('sumar/<int:op1>/<int:op2>', views.add),
    path('restar/<int:op1>/<int:op2>', views.sub),
    path('multiplicar/<int:op1>/<int:op2>', views.multi),
    path('dividir/<int:op1>/<int:op2>', views.div),

]
